$(document).ready(function(){

	var csrftoken = getCookie('csrftoken');
	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie !== '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}
    $("#orderItemBadge").text(0);
	$.ajax({
		type: "GET",
		url: "/products/categories/",
		beforeSend: function(xhr) {
			xhr.setRequestHeader('X-CSRFToken', csrftoken);
		},
		success: function(response) {
			var htmlContent = "";
			$.each(response.results,function(key,val){
				htmlContent += (
					'<div class="col-sm-3">'
					+'<div class="card img-with-text">'
					+'<a href="/product/?category='+val.uuid+'">'
					+'<img class="card-img-top" width="100%" style="height:200px;" src="'+val.image+'" alt="Card image cap">'
					+'<div class="static-text"><h2>Order Now</h2></div>'
					+'<div class="card-block">'
					+'<h4 class="card-title">'+val.name+'</h4>'
					+'</div>'
					+'</a>'
					+'</div>'
					+'</div>'
				);
			});
			$("#id_product_category").html(htmlContent);
		},
		error: function(error) {
			console.log(error);
		},
	});
	$("#order_now_btn").click(function() {
		$('html,body').animate({
			scrollTop: $("#startPoint").offset().top},
			'slow');
	});

	$("#logout-view").on("click", function(){
		$.ajax({
			type: 'GET',
			url: "/auth/logout/",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			},
			success: function(response) {
				sessionStorage.setItem('status','');
				window.location.href = "/";
			},
			error: function(error) {
				$("#loginerror").html(error.responseJSON.non_field_errors);
			}
		});
		return false;
	});
});
var csrftoken = getCookie('csrftoken');
function getCookie(name) {
	var cookieValue = null;
	if (document.cookie && document.cookie !== '') {
		var cookies = document.cookie.split(';');
		for (var i = 0; i < cookies.length; i++) {
			var cookie = jQuery.trim(cookies[i]);
			if (cookie.substring(0, name.length + 1) == (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}
