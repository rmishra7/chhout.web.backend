$(document).ready(function(){
	
	var csrftoken = getCookie('csrftoken');
	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	};
	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	$('input[type=radio][name=payment_mode]').change(function() {
		if ($(this).val() == "submit") {
			$("#id_payNow").show();
			$("#id_placeOrder").hide();
		} else {
			$("#id_payNow").hide();
			$("#id_placeOrder").show();
		}
	});
	$("#id_placeOrder").on("click", function(){
		if ($("#id_placeOrder").attr("type") == "submit") {
			payment_mode = "payu";
		} else {
			payment_mode = "cod";
		}
		$.ajax({
			type: "PUT",
			url: "/payments/"+getParameterByName('order__ppk')+"/",
			data: {"payment_mode": payment_mode},
			headers: {
				"X-CSRFToken": csrftoken
			},
			success: function(response) {
				if (payment_mode == "cod") {
					window.location.href = "/order-complete/?order__ppk="+getParameterByName('order__ppk')+"&payment_mode=cod";
				}
			},
			error: function(error){

			},
		});
		return false;
	});
});