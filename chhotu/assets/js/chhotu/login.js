$(document).ready(function(){
	
	var csrftoken = getCookie('csrftoken');
	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	};

	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		    results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	if (getParameterByName("nextUrl", "") != ""){
		$("#id_registerUser").attr('href', "/register/?nextUrl="+getParameterByName('nextUrl')+"&cart_ppk="+getParameterByName('cart_ppk'));
	}

	$("#login_form").submit(function(event){
		$("#loginerror").html("");
		$.ajax({
			type: 'POST',
			url: "/auth/login/",
			data: $(this).serialize(),
			beforeSend: function(xhr) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			},
			success: function(response) {
				if (getParameterByName("nextUrl", "")) {
					window.location.href = getParameterByName("nextUrl", "")+"?cart_ppk="+getParameterByName("cart_ppk");
				} else {
					window.location.href = "/";
				}
			},
			error: function(error) {
				$("#loginerror").html(error.responseJSON.non_field_errors);
			}
		});
		return false;
	});
});