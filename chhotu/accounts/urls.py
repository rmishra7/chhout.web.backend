from django.conf.urls import url
from accounts import apis, views

urlpatterns = [
    url(r'^register/$', apis.Register.as_view(), name="register"),
    url(r'^login/$', apis.Login.as_view(), name="login"),
    url(r'^logout/$', apis.LogOut.as_view(), name='logout'),
    url(r'^address/$', apis.UserAddressApi.as_view(), name="user-address-api"),
    url(r'^address/(?P<pk>\d+)/$', apis.UserAddressDetail.as_view(), name="api_update_useraddress"),
    url(r'^forgot-password/$', apis.ForgotPasswordApi.as_view(), name="forgot-password-api"),
    url(r'^password-reset/(?P<username>.*)/(?P<activation_key>.*)/$', apis.ResetPasswordApi.as_view(), name="api_reset_password"),
    url(r'^authenticate/$', apis.UserAuthenticate.as_view(), name="api_authenticate"),
]
