
from django.utils.translation import ugettext_lazy as _
from django.contrib import auth
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404

from rest_framework import generics, response, status, permissions, exceptions

from .permissions import IsNotAuthenticated
from .serializers import (
    RegisterSerializer, LoginSerializer, LogOutSerializer, UserAddressSerializer,
    ForgotPasswordSerializer, ResetPasswordSerializer)
from .models import UserAddress, PasswordReset, Profile


class Register(generics.CreateAPIView):
    """
    api to register user
    """
    serializer_class = RegisterSerializer
    permission_classes = (IsNotAuthenticated, )

    def create(self, request, format=None):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            self.perform_create(serializer)
            response_data = {
                'message': _('Logged in successfully'),
            }
            return response.Response(response_data, status=status.HTTP_201_CREATED)
        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def perform_create(self, serializer):
        serializer.save()
        auth.login(self.request, serializer.instance)


class Login(generics.GenericAPIView):

    """
    Signin using your email address and password
    """
    serializer_class = LoginSerializer
    permission_classes = (IsNotAuthenticated, )

    def post(self, request, format=None):
        """
        Authenticate User againest credentials & return Authorization token
        """
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            auth.login(request, serializer.instance)
            response_data = {
                'message': _('Logged in successfully'),
            }
            return response.Response(response_data, status=status.HTTP_200_OK)
        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LogOut(generics.GenericAPIView):
    """
    api to unauthenticate/logout the user
    """
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = LogOutSerializer

    def get(self, request, *args, **kwargs):
        auth.logout(request)
        response_data = {
            'message': _('Logged out successfully'),
        }
        res = response.Response(response_data, status=status.HTTP_200_OK)
        res.delete_cookie('status')
        return res


class UserAddressApi(generics.ListCreateAPIView):
    """
    api to list/create user's address
    """
    model = UserAddress
    serializer_class = UserAddressSerializer
    page_size = 10
    permission_classes = (permissions.IsAuthenticated, )

    def get_queryset(self):
        return self.model.objects.get_my_address(self.request.user)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return response.Response(serializer.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        serializer.save()


class UserAddressDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    api to get detail/update/destroy user address
    """
    model = UserAddress
    serializer_class = UserAddressSerializer
    page_size = 10
    permission_classes = (permissions.IsAuthenticated, )
    lookup_url_kwargs = "pk"

    def get_object(self):
        instance = get_object_or_404(UserAddress, pk=self.kwargs[self.lookup_url_kwargs])
        return instance

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return response.Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete = True
        instance.save()


class ForgotPasswordApi(generics.GenericAPIView):
    """
    Request password TSPasswordRecovery

    ** Parameters **
    * email - where password recovery email will be triggered.
    """
    serializer_class = ForgotPasswordSerializer

    def post(self, request, format=None):
        """ validate email id & trigger recovery email """
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            reset = PasswordReset.objects.create_for_user(serializer.data["email"])
            url = reverse('api_reset_password', kwargs={'username': reset.user.username, 'activation_key': reset.activation_key})
            response_data = {
                'reset-link': url
            }
            # password_recovery_listener.delay(reset.user.id, reset.activation_key)
            return response.Response(response_data, status=status.HTTP_200_OK)
        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ResetPasswordApi(generics.GenericAPIView):
    """
    api to reset user passowrd for valid user and valid activation key
    """
    serializer_class = ResetPasswordSerializer
    lookup_url_kwargs = "username"
    lookup_field = "activation_key"
    permission_classes = (IsNotAuthenticated)

    def get(self, request, *args, **kwargs):
        user = get_object_or_404(Profile, username=self.kwargs[self.lookup_url_kwargs])
        instance = PasswordReset.objects.filter(user=user, activation_key=self.kwargs[self.lookup_field])
        if not instance:
            raise exceptions.PermissionDenied("Invalid password reset token")
        return response.Response(status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response_data = {
            "success": "Password updated successfully"
        }
        return response.Response(response_data, status=status.HTTP_200_OK)


class UserAuthenticate(generics.GenericAPIView):
    """
    api to check whether user is logged in or not
    """
    model = Profile

    def get(self, request, *args, **kwargs):
        response_data = {'manager': request.user.is_anonymous()}
        return response.Response(response_data, status=status.HTTP_200_OK)
