
from django.db import models
from django.db.models import query

from .mixins import DiscountMixin, OrderAmountMixin, PaymentDetailMixin


class DiscountQuerySet(query.QuerySet, DiscountMixin):
    pass


class DiscountManager(models.Manager, DiscountMixin):

    def get_queryset(self):
        return DiscountQuerySet(self.model, using=self._db).filter(delete=False)


class OrderAmountQuerySet(query.QuerySet, OrderAmountMixin):
    pass


class OrderAmountManager(models.Manager, OrderAmountMixin):

    def get_queryset(self):
        return OrderAmountQuerySet(self.model, using=self._db).filter(delete=False)


class PaymentDetailQuerySet(query.QuerySet, PaymentDetailMixin):
    pass


class PaymentDetailManager(models.Manager, PaymentDetailMixin):

    def get_queryset(self):
        return PaymentDetailQuerySet(self.model, using=self._db)
