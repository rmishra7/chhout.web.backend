
from django.conf.urls import url

from payments import apis

urlpatterns = [
    url(r'^(?P<cart_pk>\d+)/pricing/$', apis.TotalOrderCost.as_view(), name="api_total_cost"),
    url(r'^(?P<order_pk>\d+)/$', apis.PaymentOrderUpdate.as_view(), name="api_payment_update")
]
