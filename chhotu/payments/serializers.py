
from django.utils import timezone
from django.shortcuts import get_object_or_404

from rest_framework import serializers
from .models import Discount, OrderAmount


class OrderCostSerializer(serializers.Serializer):

    coupon = serializers.CharField(max_length=20)

    def validate(self, attrs):
        discount = get_object_or_404(Discount, coupon_code=attrs['coupon'])
        attrs.pop('coupon')
        if discount.start_date <= timezone.now().date() and discount.end_date >= timezone.now().date() and discount.is_active is True:
            attrs['discount'] = discount
            return attrs
        raise serializers.ValidationError("Invalid Discount Coupon")


class PaymentOrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderAmount
        exclude = ("delete", )
        read_only_fields = ("uuid", "amount", "payable_amount", "discount_coupon", "content_type", "object_id")
