from django.contrib import admin

from .models import Discount, OrderAmount

models = [Discount, OrderAmount]

admin.site.register(models)
