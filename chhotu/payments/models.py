from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import fields

import uuid

from .managers import DiscountManager, OrderAmountManager, PaymentDetailManager


class Discount(models.Model):
    """
    discount detail model
    """
    REFERAL = "Referal"
    MONETARY = "Monetary"

    COUPON_TYPE_CHOICES = [
        (REFERAL, "Referal"),
        (MONETARY, "Monetary")
    ]

    uuid = models.UUIDField(_("Unique ID"), primary_key=True, default=uuid.uuid4, editable=False)
    coupon_code = models.CharField(_("Discount Code"), max_length=40)
    amount = models.IntegerField(_("Amount"))
    coupon_type = models.CharField(_("Coupon Type"), choices=COUPON_TYPE_CHOICES, max_length=20)
    start_date = models.DateField(_("Start Date"))
    end_date = models.DateField(_("End Date"))
    is_active = models.BooleanField(_("Is Coupon Active"), default=False)
    delete = models.BooleanField(_("Delete"), default=False)

    objects = DiscountManager()

    class Meta:
        verbose_name = _("Discount")
        verbose_name_plural = _("Discounts")
        app_label = "payments"

    def __unicode__(self):
        return "%s" % (self.uuid)


class OrderAmount(models.Model):
    """
    order payment amount store
    """
    PAYU = "payu"
    COD = "cod"

    PAYMENT_MODE_CHOICES = [
        (PAYU, "payu"),
        (COD, "cod")
    ]

    uuid = models.UUIDField(_("unique ID"), primary_key=True, default=uuid.uuid4, editable=False)
    amount = models.IntegerField(_("Order Amount"))
    payable_amount = models.IntegerField(_("Payable Amount"))
    payment_mode = models.CharField(_("Payment Mode"), max_length=4, choices=PAYMENT_MODE_CHOICES)
    discount_coupon = models.ForeignKey(Discount, related_name=_("discount"), blank=True, null=True)
    limit = models.Q(app_label='orders', model='cart') | \
        models.Q(app_label='orders', model='order')
    content_type = models.ForeignKey(
        ContentType,
        verbose_name=_('content page'),
        limit_choices_to=limit,
        null=True,
        blank=True,
    )
    object_id = models.PositiveIntegerField(
        verbose_name=_('related object'),
        null=True,
    )
    content_object = fields.GenericForeignKey('content_type', 'object_id')
    delete = models.BooleanField(_("Delete"), default=False)

    objects = OrderAmountManager()

    class Meta:
        verbose_name = _("OrderAmount")
        verbose_name_plural = _("OrderAmounts")
        app_label = "payments"

    def __unicode__(self):
        return "%s" % (self.uuid)


class PaymentDetail(models.Model):
    """
    order payment detail
    """
    payu_id = models.CharField(_("Payu ID/mihpayid"), max_length=40)
    mode = models.CharField(_("Payment Mode(cc/dc/onl)"), max_length=40)
    status = models.CharField(_("Payment Status"), max_length=20)
    key = models.CharField(_("Payu Key"), max_length=10)
    txnid = models.CharField(_("Transaction ID"), max_length=64)
    amount = models.CharField(_("Amount Paid"), max_length=10)
    email = models.CharField(_("Payee Email"), max_length=30)
    hash = models.CharField(_("Payment Hash"), max_length=200)

    objects = PaymentDetailManager()

    class Meta:
        verbose_name = _("PaymentDetail")
        verbose_name_plural = _("PaymentDetails")
        app_label = "payments"

    def __unicode__(self):
        return "%s" % (self.email)
