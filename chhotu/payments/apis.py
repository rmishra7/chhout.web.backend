
from django.shortcuts import get_object_or_404

from rest_framework import generics, status, response

from .models import OrderAmount
from orders.models import Cart, Order
from .serializers import OrderCostSerializer, PaymentOrderSerializer


class TotalOrderCost(generics.GenericAPIView):

    model = OrderAmount
    serializer_class = OrderCostSerializer
    lookup_url_kwarg = "cart_pk"

    def get(self, request, *args, **kwargs):
        cart = get_object_or_404(Cart, pk=self.kwargs[self.lookup_url_kwarg])
        instance = get_object_or_404(self.model, object_id=cart.pk)
        response_data = {
            'amount': instance.payable_amount,
            'discount_coupon': instance.discount_coupon.coupon_code if instance.discount_coupon is not None else '',
            'discount_amount': instance.discount_coupon.amount if instance.discount_coupon is not None else ''
        }
        return response.Response(response_data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        cart = get_object_or_404(Cart, pk=self.kwargs[self.lookup_url_kwarg])
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = get_object_or_404(self.model, object_id=cart.pk)
        instance.discount_coupon = serializer.validated_data['discount']
        instance.payable_amount = instance.amount - serializer.validated_data['discount'].amount
        instance.save()
        response_data = {
            'amount': instance.payable_amount,
            'discount_coupon': instance.discount_coupon.coupon_code if instance.discount_coupon is not None else '',
            'discount_amount': instance.discount_coupon.amount if instance.discount_coupon is not None else ''
        }
        return response.Response(response_data, status=status.HTTP_200_OK)


class PaymentOrderUpdate(generics.RetrieveUpdateAPIView):

    model = OrderAmount
    serializer_class = PaymentOrderSerializer
    lookup_url_kwarg = "order_pk"

    def get_object(self):
        order = Order.objects.get(pk=self.kwargs[self.lookup_url_kwarg])
        instance = get_object_or_404(self.model, object_id=order.id)
        return instance

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return response.Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return response.Response(serializer.data, status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        serializer.save()
