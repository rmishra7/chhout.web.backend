from django.contrib import admin

from .models import Product, ProductCategory

models = [Product, ProductCategory]

admin.site.register(models)
