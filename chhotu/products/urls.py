
from django.conf.urls import url
from products import apis


urlpatterns = [
    url(r'^$', apis.ProductsApi.as_view(), name="api_products_list"),
    url(r'^categories/$', apis.ProductCategoryApi.as_view(), name="api_product_categories")
]
