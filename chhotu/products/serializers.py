
from rest_framework import serializers
from .models import Product, ProductCategory


class ProductCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductCategory


class ProductSerializer(serializers.ModelSerializer):
    """
    serializer for product list/create api
    """
    categories = ProductCategorySerializer(source='category', read_only=True)

    class Meta:
        model = Product
        extra_kwargs = {
            'category': {'write_only': True}
        }
