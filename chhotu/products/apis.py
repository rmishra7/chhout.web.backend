
from rest_framework import generics

from .models import Product, ProductCategory
from .serializers import ProductSerializer, ProductCategorySerializer


class ProductsApi(generics.ListAPIView):
    """
    api to list down products
    """
    model = Product
    serializer_class = ProductSerializer
    # queryset = model.objects.all()

    def get_queryset(self):
        print "##############################"
        category = self.request.query_params.get('category', None)
        if category is not None:
            return self.model.objects.filter(category__uuid=category)
        return self.model.objects.all()


class ProductCategoryApi(generics.ListAPIView):
    """
    api to return list of product categories
    """
    model = ProductCategory
    serializer_class = ProductCategorySerializer
    queryset = model.objects.all()
