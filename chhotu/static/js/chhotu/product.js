$(document).ready(function(){

	var csrftoken = getCookie('csrftoken');
	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	};
	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		    results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	get_products(getParameterByName('category'));
	$.ajax({
		type: "GET",
		url: "/products/categories/",
		beforeSend: function(xhr) {
			xhr.setRequestHeader('X-CSRFToken', csrftoken);
		},
		success: function(response) {
			var htmlContent = "";
			$.each(response.results,function(key,val){
				htmlContent += (
					'<li class="list-group-item" id="'+val.uuid+'">'+val.name+'</li>'
				);
			});
			$("#id_product_categories").html(htmlContent);
			$("#"+getParameterByName('category')).addClass('category_selected');
		},
		error: function(error) {
			console.log(error);
		},
	});
	function get_products(category) {
		$('#id_product_categories').children().removeClass('category_selected');
		$("#"+category).addClass('category_selected');
		$.ajax({
			type: "GET",
			url: "/products/?category="+category,
			beforeSend: function(xhr) {
				xhr.setRequestHeader('X-CSRFToken', csrftoken);
			},
			success: function(response) {
				$("#id_product_name").html(response.results[0].categories['name']);

				var htmlContent = "";
				$.each(response.results,function(key,val){
					htmlContent += (
						'<div class="col-sm-3">'
						+'<div class="card">'
						+'<img class="card-img-top" height="150px" width="100%" src="'+val.logo+'" alt="Card image cap">'
						+'<form id="orderProductForm" data-product="'+val.id+'" class="card-block pt-10">'
						+'<p class="card-title text-xs-center">'+val.name+'</p>'
						+'<hr style="margin-top: 0;"/>'
						+'<h5 class="text-xs-left" style="display:inline-block"><i class="fa fa-inr" aria-hidden="true"></i> '+val.price+'</h5>'
						+'<select class="form-control" style="width: 60%;float: right;"><option value="">Quantity</option>'
						+'<option value="1">1</option>'
						+'<option value="2">2</option>'
						+'<option value="3">3</option>'
						+'<option value="4">4</option>'
						+'<option value="5">5</option>'
						+'<option value="6">6</option>'
						+'<option value="7">7</option>'
						+'<option value="8">8</option>'
						+'<option value="9">9</option>'
						+'<option value="10">10</option>'
						+'</select>'
						+'<div style="text-align: center;padding-top: 20px;">'
						+'<span class="text-xs-right"><button id="orderFormBtn" class="btn btn-primary btn-sm" disabled>Add to cart!</button></span>'
						+'</div>'
						+'</form>'
						+'</div>'
						+'</div>'
					);
				});
				$("#id_product_list").html(htmlContent);
			},
			error: function(error) {
				console.log(error);
			},
		});
	};
	$("#logout-view").on("click", function(){
		$.ajax({
			type: 'GET',
			url: "/auth/logout/",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			},
			success: function(response) {
				sessionStorage.setItem('status','');
				window.location.href = "/";
			},
			error: function(error) {
				$("#loginerror").html(error.responseJSON.non_field_errors);
			}
		});
		return false;
	});
	// $.ajax({
	// 	url: "/orders/carts/",
	// 	type: "GET",
	// 	beforeSend: function(xhr) {
	// 		xhr.setRequestHeader("X-CSRFToken", csrftoken);
	// 	},
	// 	success: function(response) {
	// 		$("#orderItemBadge").text(response.count);
	// 	}
	// });
});
var csrftoken = getCookie('csrftoken');
function getCookie(name) {
	var cookieValue = null;
	if (document.cookie && document.cookie != '') {
		var cookies = document.cookie.split(';');
		for (var i = 0; i < cookies.length; i++) {
			var cookie = jQuery.trim(cookies[i]);
			if (cookie.substring(0, name.length + 1) == (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
};
$('html').on("change", "#orderProductForm select", function () {
	$(this).parent().find("#orderFormBtn").prop('disabled', false);
});
$('html').on("click", "#id_product_categories li", function(){
	$('#id_product_categories').children().removeClass('category_selected');
	$(this).addClass('category_selected');
	$.ajax({
		type: "GET",
		url: "/products/?category="+$(this).attr('id'),
		beforeSend: function(xhr) {
			xhr.setRequestHeader('X-CSRFToken', csrftoken);
		},
		success: function(response) {
			$("#id_product_name").html(response.results[0].categories['name']);

			var htmlContent = "";
			$.each(response.results,function(key,val){
				htmlContent += (
					'<div class="col-sm-3">'
					+'<div class="card">'
					+'<img class="card-img-top" height="150px" width="100%" src="'+val.logo+'" alt="Card image cap">'
					+'<form id="orderProductForm" data-product="'+val.id+'" class="card-block pt-10">'
					+'<p class="card-title text-xs-center">'+val.name+'</p>'
					+'<hr style="margin-top: 0;"/>'
					+'<h5 class="text-xs-left" style="display:inline-block"><i class="fa fa-inr" aria-hidden="true"></i> '+val.price+'</h5>'
					+'<select class="form-control" style="width: 60%;float: right;"><option value="">Quantity</option>'
					+'<option value="1">1</option>'
					+'<option value="2">2</option>'
					+'<option value="3">3</option>'
					+'<option value="4">4</option>'
					+'<option value="5">5</option>'
					+'<option value="6">6</option>'
					+'<option value="7">7</option>'
					+'<option value="8">8</option>'
					+'<option value="9">9</option>'
					+'<option value="10">10</option>'
					+'</select>'
					+'<div style="text-align: center;padding-top: 20px;">'
					+'<span class="text-xs-right"><button id="orderFormBtn" class="btn btn-primary btn-sm" disabled>Add to cart!</button></span>'
					+'</div>'
					+'</form>'
					+'</div>'
					+'</div>'
				);
			});
			$("#id_product_list").html(htmlContent);
		},
		error: function(error) {
			console.log(error);
		},
	});
});
$('html').on("submit", "#orderProductForm", function () {
	window.orderFormVar = $(this);
	$.ajax({
		type: "POST",
		url: "/orders/carts/",
		beforeSend: function(xhr) {
			xhr.setRequestHeader("X-CSRFToken", csrftoken);
		},
		success: function(response) {
			$("#id_cart_btn").attr('href', '/cart/?cart_ppk='+response['id']);
			$.ajax({
				type: "POST",
				url: "/orders/carts/"+response['id']+"/products/",
				data: {
					'product': orderFormVar.attr('data-product'),
					'quantity': orderFormVar.find('select').val(),
				},
				beforeSend: function(xhr) {
					xhr.setRequestHeader("X-CSRFToken", csrftoken);
				},
				success: function(response) {
					$("#orderItemBadge").text(parseInt($("#orderItemBadge").text())+1);
					orderFormVar.find('select').prop('selectedIndex',0);
					orderFormVar.find("#orderFormBtn").prop('disabled', true);
				},
				error: function(error) {
					console.log(error);
				}
			});
			return false;
		},
		error: function(error) {
			console.log(error);
		},
	});
	return false;
});