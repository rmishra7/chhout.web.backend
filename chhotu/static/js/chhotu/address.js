$(document).ready(function(){
	
	var csrftoken = getCookie('csrftoken');
	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	};
	if (!document.referrer.endsWith("/login/?nextUrl=/order/addressselect/") && !document.referrer.endsWith("/cart/") && !document.referrer.endsWith("/order/addressselect/") && !window.location.href == "/order/addressselect/") {
		window.location.href = "/";
	}	
	loaduserAddress();
	function loaduserAddress() {
		$.ajax({
			type: "GET",
			url: "/auth/address/",
			beforSend: function(xhr) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			},
			success: function(response) {
				var htmlContent = "";
				$.each(response.results,function(key,val){
					htmlContent += (
						'<div class="col-sm-4">'
							+'<div class="card" style="min-height: 210px;">'
								+'<div class="card-header" style="text-align: center;">'
								+'<button id="id_orderUserAddress" class="btn btn-success" data-address="'+val.id+'">Use This Address</button>'
								+'</div>'
								+'<div class="card-block">'
									+'<h6 class="card-text">'+val.address+', '+val.landmark+'</h6>'
									+'<p class="card-text">'+val.city+', '+val.state+', India-'+val.zipcode+'</p>'
									+'<a id="id_updateAddrBtn" data-toggle="modal" data-target="#updateAddressModal" data-address="'+val.id+'">Update</a>'
									+'<a id="id_deleteAddress" class="pull-right" data-address="'+val.id+'">Delete</a>'
								+'</div>'
							+'</div>'
						+'</div>'
					);
				});
				$("#id_userAddress").html(htmlContent);
			},
			error: function(error) {

			},
		});
	};
	$("html").on("click", "#id_updateAddrBtn", function(){
		$("#id_updateAddress").attr("data-address", $(this).attr("data-address"));
		$.ajax({
			type: "GET",
			url: "/auth/address/"+$(this).attr('data-address')+"/",
			beforSend: function(xhr) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			},
			success: function(response) {
				$("#updateAddressModal").find('textarea[name="address"]').val(response.address);
				$("#updateAddressModal").find('input[name="landmark"]').val(response.landmark);
				$("#updateAddressModal").find('input[name="city"]').val(response.city);
				$("#updateAddressModal").find('input[name="state"]').val(response.state);
				$("#updateAddressModal").find('input[name="country"]').val(response.country);
				$("#updateAddressModal").find('input[name="zipcode"]').val(response.zipcode);
			},
			error: function(error){

			},
		});
		return false;
	});
	$("html").on("click", "#id_deleteAddress", function(){
		$.ajax({
			type: "DELETE",
			url: "/auth/address/"+$(this).attr('data-address')+"/",
			headers: {
				"X-CSRFToken": csrftoken
			},
			success: function(response) {
				loaduserAddress();
			},
			error: function(response) {

			},
		});
		return false;
	});
	$("#id_addNewAddress").submit(function(event){
		$.ajax({
			type: "POST",
			url: "/auth/address/",
			data: $(this).serialize(),
			headers: {
				"X-CSRFToken": csrftoken
			},
			success: function(response) {
				loaduserAddress();
				document.getElementById("id_addNewAddress").reset();
			},
			error: function(error){

			},
		});
		return false;
	});
	$("#id_updateAddress").submit(function(event){
		$.ajax({
			type: "PUT",
			url: "/auth/address/"+$(this).attr('data-address')+"/",
			data: $(this).serialize(),
			headers: {
				"X-CSRFToken": csrftoken
			},
			success: function(response) {
				loaduserAddress();
				document.getElementById("id_addNewAddress").reset();
				$("#updateAddressModal").modal('toggle');
			},
			error: function(error){

			},
		});
		return false;
	});
	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		    results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	$("html").on("click", "#id_orderUserAddress", function(){
		$.ajax({
			type: "POST",
			url: "/orders/",
			data: {'useraddress': $(this).attr("data-address")},
			headers: {
				"X-CSRFToken": csrftoken
			},
			success: function(response) {
				order__ppk = response['id'];
				$.ajax({
					type: "POST",
					url: "/orders/"+response['id']+"/products/?cart_ppk="+getParameterByName('cart_ppk'),
					beforeSend: function(xhr) {
						xhr.setRequestHeader("X-CSRFToken", csrftoken);
					},
					success: function(response) {
						window.location.href = "/order/payment/?order__ppk="+order__ppk;
					},
					error: function(error) {
						console.log(error);
					}
				});
				return false;
			},
			error: function(error){

			},
		});
		return false;
	});
});