$(document).ready(function(){
	
	var csrftoken = getCookie('csrftoken');
	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	};
	getOrdersFunction("Confirmed");
	$("#id_selected_status").text("Confirmed Orders");
	$(".orderStatusBtn").on("click", function(){
		$("#id_selected_status").text($(this).attr('data-status-attr')+" Orders");
		getOrdersFunction($(this).attr('data-status-attr'));
	});
	function getOrdersFunction(status) {
		$.ajax({
			type: "GET",
			url: "/orders/?delivery_status="+status,
			beforSend: function(xhr) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			},
			success: function(response) {
				var htmlContent1 = "";
				var htmlContent2 = "";
				if (response.results.length == 0){
					$("#id_orders_list").html("");
				}
				$.each(response.results,function(key,val){
					$.ajax({
						type: "GET",
						url: "/orders/"+val.id+"/products/",
						beforSend: function(xhr) {
							xhr.setRequestHeader("X-CSRFToken", csrftoken);
						},
						success: function(response) {
							console.log("inide the rs1");
							console.log(response);
							$.each(response.results,function(key,val){
								htmlContent1 += (
									'<p>'+val.product.name+': '+val.quantity+'</p>'
								);
								htmlContent2 += (
									'<tr style="padding-bottom: 20px;border-bottom: 3px solid grey;">'
										+'<td><p>'+val.order.user.name+'</p><p>'+val.order.user.email+'</p><p>'+val.order.user.contact_no+'</p></td>'
										+'<td><p>'+val.order.address.address+'<p><p><p>'+val.order.address.city+', '+val.order.address.state+', '+val.order.address.country+'-'+val.order.address.zip_code+'</><p>'+val.order.address.landmark+'</p></td>'
										+'<td>'+htmlContent1+'</td>'
										+'<td>'
											+'<select class="btn btn-primary updateOrderStatus" order-attr-id='+val.order.id+' style="width: 80%;">'
												+'<option value="Confirmed">Confirmed</option>'
												+'<option value="Inprogress">InProgress</option>'
												+'<option value="Delivered">Delivered</option>'
												+'<option value="Cancelled">Cancelled</option>'
											+'</select>'
										+'</td>'
									+'</tr>'
								);
							});
							$("#id_orders_list").html(htmlContent2);
						},
						error: function(error) {

						},
					});
					return false;
				});
			},
			error: function(error) {

			},
		});
		return false;
	};
	$("html").on("change", ".updateOrderStatus", function(){
		var order = $(this).attr("order-attr-id");
		var status = $("option:selected", this).val();
		$.ajax({
			url: '/orders/'+order+"/",
			method: "GET",
			headers: {
				"X-CSRFToken": csrftoken
			},
			success: function(response) {
				console.log(response);
				console.log(order+status);
				updateStatus(order, status, response);
			},
			error: function(error){

			},
		});
		return false;
		});
		function updateStatus(order, status, response) {
		$.ajax({
			url: '/orders/'+order+"/",
			method: "PUT",
			data: {'delivery_status': status, 'useraddress': response['address'].id},
			headers: {
				"X-CSRFToken": csrftoken
			},
			success: function(response) {
				getOrdersFunction($("#id_selected_status").text().split(" ")[0]);
			},
			error: function(error){

			},
		});
		return false;
		};
});
