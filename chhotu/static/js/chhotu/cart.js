$(document).ready(function(){
	
	var csrftoken = getCookie('csrftoken');
	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	};
	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		    results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	loadCartData(getParameterByName('cart_ppk'));
	function loadCartData(cart) {
		loadCartItems(cart);
		loadPricing(cart);
	};
	function loadCartItems(cart) {
		$.ajax({
			type: "GET",
			url: "/orders/carts/"+cart+"/products/",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			},
			success: function(response) {
				var htmlContent = "";
				$.each(response.results,function(key,val){
					htmlContent += (
						'<div class="col-sm-12">'
						+'<div class="card card-block">'
						+'<div class="media">'
						+'<a class="media-left" href="#">'
						+'<img class="media-object" src="'+val.products['logo']+'" alt="Generic placeholder image" style="width: 100px; height: 100px;">'
						+'</a>'
						+'<div class="media-body">'
						+'<h5 class="media-heading font-weight-bold display-inline">'+val.products['name']+'</h5>'
						+'<h5 class="pull-right font-weight-bold">Price Rs. '+val.products['price']+'</h5>'
						+'<p>Quantity: <span id="id_productQuantity">'+val.quantity+'</span></p>'
						+'<div class="mt-25">'
						+'<hr/>'
						+'<span style="color: #0275d8;cursor: pointer;" class="card-link pull-right dropCartItem" data-product="'+val.uuid+'">Remove</span>'
						+'<a class="updateOrder" data-toggle="collapse" href="#collapseQty'+val.uuid+'">Edit Item</a>'
						+'<div class="collapse" id="collapseQty'+val.uuid+'">'
						+'<form class="updateOrderForm" data-product="'+val.uuid+'">'
						+'<div class="form-group">'
						+'<label for="exampleInputEmail1">Quantity</label>'
						+'<input type="number" name="quantity" class="form-control" placeholder="Enter quantity" value="'+val.quantity+'">'
						+'</div>'
						+'<button type="submit" class="btn btn-primary">Submit</button>'
						+'</form>'
						+'</div>'
						+'</div>'
						+'</div>'
						+'</div>'
						+'</div>'
						+'</div>'
						+'</div>'
					);
				});
				$("#id_productItem").html(htmlContent);				
			},
			error: function(error) {
				console.log(error);
			}
		});
		return false;
	};
	function loadPricing(cart) {
		$.ajax({
			type: "GET",
			url: "/payments/"+cart+"/pricing/",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			},
			success: function(response) {
				$("#id_cartTotal,#id_orderTotal").text(response['amount']);
			},
			error: function(error) {
				console.log(error);
			},
		});
		return false;
	};
	$("#id_apply_discount").on("click", function(){
		if ($("#id_discount_input").val() != '') {
			$.ajax({
				type: "POST",
				url: "/payments/"+getParameterByName('cart_ppk')+"/pricing/",
				data: {
					'coupon': $("#id_discount_input").val()
				},
				beforeSend: function(xhr) {
					xhr.setRequestHeader("X-CSRFToken", csrftoken);
				},
				success: function(response) {
					$("#id_orderTotal").text(response['amount']);
				},
				error: function(error) {
					$("#id_discount_error").text("Invalid Coupon.");
				},
			});
			return false;
		}
	});
	$("#id_placeOrder").on("click", function(){
		if (sessionStorage.getItem('_auth_flag') == "loggedIn") {
			window.location.href = "/order/addressselect/?cart_ppk="+getParameterByName('cart_ppk');
		} else {
			window.location.href = "/login/?nextUrl=/order/addressselect/&cart_ppk="+getParameterByName('cart_ppk');
		}
	});
	$("html").on("submit", ".updateOrderForm", function(){
		window.updateOrderFormVar = $(this);
		$.ajax({
			method: "PUT",
			url: "/orders/carts/"+getParameterByName('cart_ppk')+"/products/"+$(this).attr('data-product')+"/",
			data: $(this).serialize(),
			beforSend: function(xhr){
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			},
			success: function(response) {
				loadCartData(response['cart']['id']);
			},
			error: function(error){

			},
		});
		return false;
	});
	$("html").on("click", ".dropCartItem", function(){
		$.ajax({
			type: "DELETE",
			url: "/orders/carts/"+getParameterByName('cart_ppk')+"/products/"+$(this).attr('data-product')+"/",
			beforSend: function(xhr){
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			},
			success: function(response) {
				loadCartData(getParameterByName('cart_ppk'));
			},
			error: function(error){

			},
		});
		return false;
	});
});
var csrftoken = getCookie('csrftoken');
function getCookie(name) {
	var cookieValue = null;
	if (document.cookie && document.cookie != '') {
		var cookies = document.cookie.split(';');
		for (var i = 0; i < cookies.length; i++) {
			var cookie = jQuery.trim(cookies[i]);
			if (cookie.substring(0, name.length + 1) == (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
};

