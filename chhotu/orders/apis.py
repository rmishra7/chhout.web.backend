
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.db.models import Count
from django.contrib.contenttypes.models import ContentType

from rest_framework import generics, response, status, permissions
from datetime import timedelta

from .models import Cart, CartDetail, Order, OrderDetail
from payments.models import OrderAmount
from .serializers import (
    CartSerializer, CartProductSerializer, CartProductDetailSerializer, OrderSerializer,
    PlaceOrderSerializer, placeOrderDetailSerializer, OrderProductSerializer)


class CartApi(generics.ListCreateAPIView):
    """
    api to return CartProducts
    """
    model = Cart
    serializer_class = CartSerializer

    def get_queryset(self):
        if self.request.user.is_authenticated():
            return self.model.objects.filter(user=self.request.user)
        else:
            return self.model.objects.filter(pid=self.request.COOKIES.get('urcpid', None), uid=self.request.COOKIES.get('gpduid'))

    def create(self, request, *args, **kwargs):
        try:
            instance = self.model.objects.get(pid=self.request.COOKIES.get('urcpid', None), uid=self.request.COOKIES.get('gpduid', None))
            serializer = self.get_serializer(instance, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
        except:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.validated_data['pid'] = self.request.COOKIES.get('urcpid', None)
            serializer.validated_data['uid'] = self.request.COOKIES.get('gpduid', None)
            cart = serializer.save()
            ctype = ContentType.objects.get_for_model(cart)
            OrderAmount.objects.get_or_create(content_type=ctype, object_id=cart.id, amount=0, payable_amount=0)
        return response.Response(serializer.data, status=status.HTTP_200_OK)


class CartDetailApi(generics.RetrieveUpdateDestroyAPIView):
    """
    api to update cart
    """
    model = Cart
    serializer_class = CartSerializer
    lookup_url_kwarg = "pk"

    def get_object(self):
        instance = get_object_or_404(Cart, pk=self.kwargs[self.lookup_url_kwarg])
        return instance

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return response.Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return response.Response(serializer.data, status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        serializer.save()


class AddToCart(generics.ListCreateAPIView):
    """
    api to support adding products to cart
    """
    model = CartDetail
    serializer_class = CartProductSerializer
    lookup_url_kwarg = "cart_id"
    queryset = model.objects.all()

    def get_queryset(self):
        cart = get_object_or_404(Cart, pk=self.kwargs[self.lookup_url_kwarg])
        return self.queryset.filter(cart=cart)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return response.Response(serializer.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        instance = OrderAmount.objects.get(object_id=serializer.validated_data['cart'].id)
        instance.amount = instance.amount + int(serializer.validated_data['product'].price * serializer.validated_data['quantity'])
        instance.payable_amount = instance.amount
        instance.save()
        serializer.save()


class CartProductDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    api to support cart updation and ordered products
    """
    model = CartDetail
    serializer_class = CartProductDetailSerializer
    lookup_url_kwarg = "cart_id"
    lookup_field = "product_id"

    def get_object(self):
        get_object_or_404(Cart, pk=self.kwargs[self.lookup_url_kwarg])
        instance = get_object_or_404(self.model, pk=self.kwargs[self.lookup_field])
        return instance

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return response.Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return response.Response(serializer.data, status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        cart_pro = self.get_object()
        instance = OrderAmount.objects.get(object_id=cart_pro.cart.id)
        instance.amount = instance.amount + int(cart_pro.product.price * serializer.validated_data['quantity']) - int(cart_pro.product.price * cart_pro.quantity)
        instance.payable_amount = instance.amount
        instance.save()
        serializer.save()

    def destroy(self, request, *args, **kwargs):
        cart_pro = self.get_object()
        instance = OrderAmount.objects.get(object_id=cart_pro.cart.id)
        instance.amount = instance.amount - int(cart_pro.product.price * cart_pro.quantity)
        instance.payable_amount = instance.amount
        instance.save()
        self.perform_destroy(cart_pro)
        return response.Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete = True
        instance.save()


class OrderApi(generics.ListCreateAPIView):
    """
    api to order product
    """
    model = Order
    serializer_class = OrderSerializer
    queryset = model.objects.all()
    permission_classes = (permissions.IsAuthenticated, )

    def get_queryset(self):
        if self.request.user.is_superuser:
            queryset = self.queryset
        else:
            queryset = self.queryset.filter(user=self.request.user)
        kwargs = {}
        for k, vals in self.request.GET.lists():
            for v in vals:
                kwargs[k] = v
        if not kwargs:
            return queryset
        return queryset.filter(**kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return response.Response(serializer.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        serializer.save()

    # def create(self, request, *args, **kwargs):
        # try:
        #     instance = self.model.objects.filter(user=request.user).order_by('ordered_at')[0]
        #     if instance.ordered_at + timedelta(minutes=12) >= timezone.now():
        #         instance.delivery_at = instance.delivery_at
        #         instance.save()
        #         serializer = self.get_serializer(instance, data=request.data)
        #         serializer.is_valid(raise_exception=True)
        #         serializer.save()
        #     else:
        #         serializer = self.get_serializer(data=request.data)
        #         serializer.is_valid(raise_exception=True)
        #         serializer.validated_data['delivery_at'] = timezone.now() + timedelta(minutes=49)
        #         serializer.save()
        # except:
        #     serializer = self.get_serializer(data=request.data)
        #     serializer.is_valid(raise_exception=True)
        #     serializer.validated_data['delivery_at'] = timezone.now() + timedelta(minutes=49)
        #     serializer.save()
        # return response.Response(serializer.data, status=status.HTTP_200_OK)


class OrderProduct(generics.RetrieveUpdateDestroyAPIView):
    """
    api to update cart
    """
    model = Order
    serializer_class = OrderProductSerializer
    lookup_url_kwarg = "order_pk"
    permission_classes = (permissions.IsAuthenticated, )

    def get_object(self):
        instance = get_object_or_404(self.model, pk=self.kwargs[self.lookup_url_kwarg])
        return instance

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return response.Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return response.Response(serializer.data, status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        serializer.save()


class PlaceOrderApi(generics.ListCreateAPIView):
    """
    api to order product
    """
    model = OrderDetail
    serializer_class = PlaceOrderSerializer
    lookup_url_kwarg = "order_pk"
    queryset = model.objects.all()
    permission_classes = (permissions.IsAuthenticated, )

    def get_queryset(self):
        order = get_object_or_404(Order, id=self.kwargs[self.lookup_url_kwarg])
        queryset = self.model.objects.filter(order=order)
        kwargs = {}
        for k, vals in self.request.GET.lists():
            for v in vals:
                kwargs[k] = v
        if not kwargs:
            return queryset
        return queryset.filter(**kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        response_data = {
            'message': 'Order Placed Successfully'
        }
        return response.Response(response_data, status=status.HTTP_200_OK)

    def perform_create(self, serializer):
        obj = Cart.objects.get(pk=self.request.GET.get('cart_ppk'))
        items = CartDetail.objects.filter(cart=obj)
        order = Order.objects.get(pk=self.kwargs[self.lookup_url_kwarg])
        for item in items:
            OrderDetail.objects.create(order=order, product=item.product, quantity=item.quantity)
        ctype = ContentType.objects.get_for_model(order)
        order_amount = OrderAmount.objects.get(object_id=items[0].cart.id)
        order_amount.content_type = ctype
        order_amount.object_id = order.id
        order_amount.save()
        items.delete()
        obj.delete = True
        obj.save()


class AdminOrderList(generics.ListAPIView):
    """
    api to support for list of products for admin
    """
    model = Order
    serializer_class = OrderSerializer
    permission_classes = (permissions.IsAuthenticated, )
    # queryset = model.objects.all()

    def get_queryset(self):
        queryset = self.model.objects.all()
        query = {}
        for k, vals in self.request.GET.lists():
            for v in vals:
                query[k] = v
        if not query:
            results = queryset
        else:
            results = queryset.filter(**query)
        return results.annotate(dcount=Count('user__email'))
