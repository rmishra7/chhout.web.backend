from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _

import uuid

from products.models import Product
from accounts.models import Profile, UserAddress
from .managers import CartManager, OrderManager, OrderDetailManager, CartDetailManager


class Cart(models.Model):
    """
    cart model to store cart details
    """
    user = models.ForeignKey(Profile, related_name=_("order_by"), blank=True, null=True)
    pid = models.CharField(_("Cart Session Product ID"), max_length=90)
    uid = models.CharField(_("Cart Session unique ID"), max_length=90)
    added_at = models.DateTimeField(_("Added Datetime"), auto_now_add=True)
    special_comment = models.TextField(_("Special Comment"), blank=True, null=True)
    delete = models.BooleanField(_("Delete"), default=False)

    objects = CartManager()

    class Meta:
        verbose_name = _("Cart")
        verbose_name_plural = _("Carts")
        app_label = "orders"

    def __unicode__(self):
        return "%s" % (self.pid)


class CartDetail(models.Model):
    """
    model to store cart data entry
    """
    cart = models.ForeignKey(Cart, related_name=_("cart_detail"))
    product = models.ForeignKey(Product, related_name=_("product_cart"))
    quantity = models.IntegerField(_("product Quantity"))
    uuid = models.UUIDField(_("Unique ID"), primary_key=True, default=uuid.uuid4, editable=False)
    delete = models.BooleanField(_("Delete"), default=False)

    objects = CartDetailManager()

    class Meta:
        verbose_name = _("CartDetail")
        verbose_name_plural = _("CartDetails")
        app_label = "orders"

    def __unicode__(self):
        return "%s" % str(self.product.name)


class Order(models.Model):
    """
    model to store order and its details
    """
    CONFIRMED = "Confirmed"
    INPROGRESS = "Inprogress"
    DELIVERED = "Delivered"
    CANCELLED = "Cancelled"

    ORDER_DELIVERY_STATUS = [
        (CONFIRMED, "Order Confirmed"),
        (INPROGRESS, "In Progress"),
        (DELIVERED, "Order Delivered"),
        (CANCELLED, "Order Cancelled")
    ]
    delivery_status = models.CharField(_("Delivery Status"), max_length=15, default=CONFIRMED, choices=ORDER_DELIVERY_STATUS)
    uuid = models.CharField(max_length=40, default=uuid.uuid4)
    user = models.ForeignKey(Profile, related_name=_("user_ordered"))
    useraddress = models.ForeignKey(UserAddress, related_name=_("order_address"))
    special_comment = models.TextField(_("Special Comment"), blank=True, null=True)
    ordered_at = models.DateTimeField(_("Order Placed At"), auto_now_add=True)
    delivery_at = models.DateTimeField(_("Order Delivery At"))
    delete = models.BooleanField(_("Delete"), default=False)

    objects = OrderManager()

    class Meta:
        verbose_name = _("Order")
        verbose_name_plural = _("Orders")
        app_label = "orders"


class OrderDetail(models.Model):
    """
    models to store final order data
    """
    order = models.ForeignKey(Order, related_name=_("order_detail"))
    product = models.ForeignKey(Product, related_name=_("orders"))
    quantity = models.IntegerField(_("Product Quantity"))
    uuid = models.UUIDField(_("Unique ID"), primary_key=True, default=uuid.uuid4, editable=False)
    delete = models.BooleanField(_("Delete"), default=False)

    objects = OrderDetailManager()

    class Meta:
        verbose_name = _("OrderDetail")
        verbose_name_plural = _("OrderDetails")
        app_label = "orders"

    def __unicode__(self):
        return "%s" % str(self.product.name)
