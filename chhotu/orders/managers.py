
from django.db.models.query import QuerySet
from django.db import models

from .mixins import CartMixin, CartDetailMixin, OrderMixin, OrderDetailMixin


class CartQuerySet(QuerySet, CartMixin):
    pass


class CartManager(models.Manager, CartMixin):

    def get_queryset(self):
        return CartQuerySet(self.model, using=self._db).filter(delete=False)


class CartDetailQuerySet(QuerySet, CartDetailMixin):
    pass


class CartDetailManager(models.Manager, CartDetailMixin):

    def get_queryset(self):
        return CartDetailQuerySet(self.model, using=self._db).filter(delete=False)


class OrderQuerySet(QuerySet, OrderMixin):
    pass


class OrderManager(models.Manager, OrderMixin):

    def get_queryset(self):
        return OrderQuerySet(self.model, using=self._db).filter(delete=False)


class OrderDetailQuerySet(QuerySet, OrderDetailMixin):
    pass


class OrderDetailManager(models.Manager, OrderDetailMixin):

    def get_quesryset(self):
        return OrderDetailQuerySet(self.model, using=self._db).filter(delete=False)
