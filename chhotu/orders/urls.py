from django.conf.urls import url

from orders import apis

urlpatterns = [
    url(r'^carts/$', apis.CartApi.as_view(), name="api_cartproducts"),
    url(r'^carts/(?P<pk>\d+)/$', apis.CartDetailApi.as_view(), name="cart-detail"),
    url(r'^carts/(?P<cart_id>\d+)/products/$', apis.AddToCart.as_view(), name="api_addtocart"),
    url(r'^carts/(?P<cart_id>\d+)/products/(?P<product_id>[a-z\0-9\-]+)/$', apis.CartProductDetail.as_view(), name="cart-detail"),
    url(r'^$', apis.OrderApi.as_view(), name="api_order-product"),
    url(r'^admin-access/$', apis.AdminOrderList.as_view(), name="api_admin_accessorders"),
    url(r'^(?P<order_pk>\d+)/$', apis.OrderProduct.as_view(), name="api_order_detail"),
    url(r'^(?P<order_pk>\d+)/products/$', apis.PlaceOrderApi.as_view(), name="order-detail"),
]
