
from django.shortcuts import get_object_or_404

from datetime import timedelta, datetime
from rest_framework import serializers

from accounts.serializers import ProfileMiniSerializer, UserAddressSerializer
from products.serializers import ProductSerializer
from .models import Cart, CartDetail, Order, OrderDetail


class CartSerializer(serializers.ModelSerializer):

    user = ProfileMiniSerializer(read_only=True)

    def validate(self, attrs):
        view = self.context.get('view')
        if view.request.user.is_authenticated():
            attrs['user'] = view.request.user
        return attrs

    class Meta:
        model = Cart
        read_only_fields = ('added_at', 'user', 'uid', 'pid')
        exclude = ('delete', )


class CartProductSerializer(serializers.ModelSerializer):
    """
    serializer for add to cart api
    """
    cart = CartSerializer(read_only=True)
    products = ProductSerializer(read_only=True, source='product')

    def validate(self, attrs):
        view = self.context.get('view')
        cart = get_object_or_404(Cart, pk=view.kwargs[view.lookup_url_kwarg])
        attrs['cart'] = cart
        return attrs

    class Meta:
        model = CartDetail
        fields = ('uuid', 'cart', 'product', 'quantity', 'products')
        read_only_fields = ('cart', )


class CartProductDetailSerializer(serializers.ModelSerializer):
    """
    serializer for update/detail/destroy cart instance
    """
    cart = CartSerializer(read_only=True)
    products = ProductSerializer(read_only=True, source='product')

    class Meta:
        model = CartDetail
        fields = ('uuid', 'cart', 'product', 'quantity', 'products')
        read_only_fields = ('cart', 'uuid', 'product')


class OrderSerializer(serializers.ModelSerializer):
    """
    serializers for order
    """
    user = ProfileMiniSerializer(read_only=True)
    address = UserAddressSerializer(read_only=True, source='useraddress')

    def validate(self, attrs):
        view = self.context.get('view')
        attrs['user'] = view.request.user
        attrs['delivery_at'] = datetime.now() + timedelta(minutes=49)
        return attrs

    class Meta:
        model = Order
        read_only_fields = ('uuid', 'delivery_status', 'ordered_at', 'delivery_at')
        exclude = ('delete', )


class OrderProductSerializer(serializers.ModelSerializer):
    """
    serializers for order
    """
    user = ProfileMiniSerializer(read_only=True)
    address = UserAddressSerializer(read_only=True, source='useraddress')

    # def validate(self, attrs):
    #     view = self.context.get('view')
    #     attrs['user'] = view.request.user
    #     return attrs

    class Meta:
        model = Order
        read_only_fields = ('ordered_at', 'delivery_at')
        exclude = ('delete', )
        extra_kwargs = {
            'useraddress': {'write_only': True}
        }


class PlaceOrderSerializer(serializers.ModelSerializer):
    """
    serializers for order detail/update/destroy
    """
    order = OrderSerializer(read_only=True)
    product = ProductSerializer(read_only=True)
    # user = ProfileMiniSerializer(read_only=True)

    class Meta:
        model = OrderDetail
        fields = ('order', 'product', 'quantity', 'uuid',)
        read_only_fields = ('order', 'product', 'quantity', 'uuid',)
        # read_only_fields = ('amount', 'user', 'ordered_at', 'delivery_at')


class placeOrderDetailSerializer(serializers.ModelSerializer):

    product = ProductSerializer(read_only=True)

    def validate(self, attrs):
        attrs['amount'] = int(attrs.get('quantity')) * int(self.instance.product.price)
        return attrs

    class Meta:
        model = Cart
        fields = ('id', 'product', 'quantity', 'amount', 'user', 'uid', 'pid')
        read_only_fields = ('amount', 'user', 'pid', 'uid')
