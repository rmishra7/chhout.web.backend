# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-10-03 01:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_auto_20161001_1622'),
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CartDetail',
            fields=[
                ('quantity', models.IntegerField(verbose_name='product Quantity')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='Unique ID')),
                ('delete', models.BooleanField(default=False, verbose_name='Delete')),
            ],
            options={
                'verbose_name': 'CartDetail',
                'verbose_name_plural': 'CartDetails',
            },
        ),
        migrations.CreateModel(
            name='OrderDetail',
            fields=[
                ('quantity', models.IntegerField(verbose_name='Product Quantity')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='Unique ID')),
                ('delete', models.BooleanField(default=False, verbose_name='Delete')),
            ],
            options={
                'verbose_name': 'OrderDetail',
                'verbose_name_plural': 'OrderDetails',
            },
        ),
        migrations.RemoveField(
            model_name='cart',
            name='amount',
        ),
        migrations.RemoveField(
            model_name='cart',
            name='product',
        ),
        migrations.RemoveField(
            model_name='cart',
            name='quantity',
        ),
        migrations.RemoveField(
            model_name='order',
            name='amount',
        ),
        migrations.RemoveField(
            model_name='order',
            name='product',
        ),
        migrations.RemoveField(
            model_name='order',
            name='quantity',
        ),
        migrations.AddField(
            model_name='cart',
            name='special_comment',
            field=models.TextField(blank=True, null=True, verbose_name='Special Comment'),
        ),
        migrations.AddField(
            model_name='order',
            name='special_comment',
            field=models.TextField(blank=True, null=True, verbose_name='Special Comment'),
        ),
        migrations.AlterField(
            model_name='order',
            name='delivery_status',
            field=models.CharField(choices=[('Confirmed', 'Order Confirmed'), ('Inprogress', 'In Progress'), ('Delivered', 'Order Delivered'), ('Cancelled', 'Order Cancelled')], default='Confirmed', max_length=15, verbose_name='Delivery Status'),
        ),
        migrations.AddField(
            model_name='orderdetail',
            name='order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='order_detail', to='orders.Order'),
        ),
        migrations.AddField(
            model_name='orderdetail',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='orders', to='products.Product'),
        ),
        migrations.AddField(
            model_name='cartdetail',
            name='cart',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cart_detail', to='orders.Cart'),
        ),
        migrations.AddField(
            model_name='cartdetail',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='product_cart', to='products.Product'),
        ),
    ]
