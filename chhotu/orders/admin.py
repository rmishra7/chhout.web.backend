from django.contrib import admin

from .models import Cart, CartDetail, Order, OrderDetail

models = [Cart, CartDetail, Order, OrderDetail]

admin.site.register(models)
