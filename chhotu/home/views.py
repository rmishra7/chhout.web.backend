from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic.base import TemplateView
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.views.generic import View
from django.template import Context
from django.template.loader import get_template
from django.core.mail import EmailMessage

import datetime
from hashlib import sha512
import uuid
from braces.views import AnonymousRequiredMixin, LoginRequiredMixin

from orders.models import Order, OrderDetail
from payments.models import OrderAmount, PaymentDetail


class RegisterView(AnonymousRequiredMixin, TemplateView):
    """
    view to render register template
    """
    authenticated_redirect_url = settings.LOGIN_REDIRECT_URL
    template_name = "accounts/register.html"


class LoginView(AnonymousRequiredMixin, TemplateView):
    """
    view to render login template
    """
    authenticated_redirect_url = settings.LOGIN_REDIRECT_URL
    template_name = "accounts/login.html"


class HomeView(TemplateView):
    """
    landing page/index page views
    """
    template_name = "home/index.html"

    def get(self, request, *args, **kwargs):
        _urcpid = uuid.uuid4()
        _gpduid = uuid.uuid4()
        max_age = 12 * 60
        expires = datetime.datetime.strftime(datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")
        response = render(request, self.template_name)
        response.set_cookie('urcpid', _urcpid, max_age=max_age, expires=expires)
        response.set_cookie('gpduid', _gpduid, max_age=max_age, expires=expires)
        return response


class ProductView(TemplateView):
    """
    product template view
    """
    template_name = "order/product.html"


class CartView(TemplateView):
    """
    cart template view
    """
    template_name = "order/cart.html"


class AddressView(LoginRequiredMixin, TemplateView):
    """
    view to render address template for user detail and update its details
    """
    login_url = "/"
    template_name = "order/address.html"

    def get(self, request, *args, **kwargs):
        response = render(request, self.template_name)
        response.delete_cookie('urcpid')
        response.delete_cookie('gpduid')
        return response


class OrderPaymentView(LoginRequiredMixin, TemplateView):
    """
    view to render order payment template
    """
    login_url = "/"
    template_name = "payments/payment.html"

    def get_context_data(self, **kwargs):
        context = super(OrderPaymentView, self).get_context_data(**kwargs)
        if not self.request.GET.get('order__ppk'):
            order = Order.objects.filter(user=self.request.user).order_by('-ordered_at')[0]
        else:
            order = Order.objects.get(id=self.request.GET.get('order__ppk'))
        context['order'] = order
        context['orders'] = OrderDetail.objects.filter(order=order)
        ctype = ContentType.objects.get_for_model(order)
        context['order_amount'] = OrderAmount.objects.get(content_type=ctype, object_id=order.id)
        context['merchant_key'] = settings.PAYU_MERCHANT_KEY
        context['merchant_salt'] = settings.PAYU_MERCHANT_SALT
        context['payment_url'] = settings.PAYU_PAYMENT_URL
        context['domain'] = self.request.build_absolute_uri("/").rstrip("/")
        key = str(settings.PAYU_MERCHANT_KEY)
        txnid = str(context["order"].uuid)
        amount = str(context["order_amount"].payable_amount)
        productinfo = str(context["order"].uuid)
        firstname = str(context["order"].user.name)
        email = str(context['order'].user.email)
        salt = str(settings.PAYU_MERCHANT_SALT)
        context['hash'] = sha512(key+"|"+txnid+"|"+amount+"|"+productinfo+"|"+firstname+"|"+email+"|"+"|"+"|"+"|"+"|"+"|"+"|"+"|"+"|"+"|"+"|"+salt).hexdigest()
        return context


class AdminOrderView(LoginRequiredMixin, TemplateView):
    """
    order review and submit order
    """
    login_url = "/"
    template_name = "admin/orders.html"


class OrderCompleteView(View):
    """
    view to render success template
    """
    def get(self, request):
        order = Order.objects.get(id=request.GET.get('order__ppk'))
        self._inform_user(order.uuid)
        context = {'status': "success"}
        return render(request, 'order/order-complete.html', context)

    def post(self, request):
        PaymentDetail.objects.create(
            payu_id=request.POST['mihpayid'], mode=request.POST['mode'], status=request.POST['status'],
            key=request.POST['key'], txnid=request.POST['txnid'], amount=request.POST['amount'],
            email=request.POST['email'], hash=request.POST['hash'])
        self._inform_user(request.POST['txnid'])
        context = {'status': request.POST['status']}
        return render(request, 'order/order-complete.html', context)

    def _inform_user(self, txnid):
        subject = "Order Detail Confirmation"
        to = [self.request.user.email, ]
        from_email = 'order@urschottu.com'
        order = Order.objects.get(uuid=txnid)
        orders = OrderDetail.objects.filter(order=order)
        ctx = {
            'user': self.request.user,
            'orders': orders
        }
        message = get_template('mail/orders.html').render(Context(ctx))
        msg = EmailMessage(subject, message, to=to, from_email=from_email)
        msg.content_subtype = 'html'
        msg.send()
        from twilio.rest import TwilioRestClient
        accountSid = settings.TWILIO_SID
        authToken = settings.TWILIO_AUTH_TOKEN
        twilioClient = TwilioRestClient(accountSid, authToken)
        myTwilioNumber = "+1 928-362-3296 "
        destCellPhone = '+91 9467116467'
        ORDER_LIST = ", ".join([item.product.name for item in orders])
        msgtext = "Hi "+self.request.user.name+" Your order " + ORDER_LIST + " has been successfully placed.All items in your ordered will be delivered within 49 mins."
        twilioClient.messages.create(body=msgtext, from_=myTwilioNumber, to=destCellPhone)
        # from sendsms.message import SmsMessage
        # message = SmsMessage(body=msgtext, from_phone='+919467116467', to=['+917061251325'])
        # message.send()


class AboutUsView(TemplateView):
    """
    view to render about us template
    """
    template_name = "home/about-us.html"


class TermsConditionView(TemplateView):
    """
    terms n condition
    """
    template_name = "home/terms-condition.html"


class PrivacyPolicyView(TemplateView):
    """
    view to render privacy policy template
    """
    template_name = "home/privacy.html"


class RefundPolicyView(TemplateView):
    """
    view to render refund policy template
    """
    template_name = "home/refund.html"


class ContactUsView(TemplateView):
    """
    view to render contact us template
    """
    template_name = "home/contact.html"
