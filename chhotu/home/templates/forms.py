
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate
from accounts.models import Profile as User
from django.contrib import auth
from django.core.exceptions import ValidationError


class OrderPaymentForm(forms.Form):
    """
    Form for login a user.
    """
    key = forms.EmailField(widget=forms.TextInput, label=_("Merhant Key"))
    txnid = forms.CharField(widget=forms.TextInput, label=_("Order UUID"))
    amount = forms.CharField(widget=forms.TextInput, label=_("Order Payable Amount"))
    productinfo = forms.CharField(widget=forms.TextInput, label=_("Order UUID"))
    firstname = forms.CharField(widget=forms.TextInput, label=_("Buyer Name"))
    email = forms.CharField(widget=forms.TextInput, label=_("Buyer Email"))
    udf1 = forms.CharField(widget=forms.TextInput, label=_("Delivery Address"))
    udf2 = forms.CharField(widget=forms.TextInput, label=_("Ordered At"))
    udf3 = forms.CharField(widget=forms.TextInput, label=_("Delivery DateTime"))
    udf4 = forms.CharField(widget=forms.TextInput, label=_("Merchant Salt"))
    phone = forms.CharField(widget=forms.TextInput, label=_("Phone"))
    surl = forms.CharField(widget=forms.TextInput, label=_("Success URL"))
    furl = forms.CharField(widget=forms.TextInput, label=_("Failure URL"))
    curl = forms.CharField(widget=forms.TextInput, label=_("Cancel URL"))
    hash = forms.CharField(widget=forms.TextInput, label=_("Hash"))
