from django.conf.urls import url
from django.views.generic.base import RedirectView
from django.views.decorators.csrf import csrf_exempt

from home import views

urlpatterns = [
    url(r'^register/$', views.RegisterView.as_view(), name="registration_view"),
    url(r'^login/$', views.LoginView.as_view(), name="login_view"),

    url(r'^$', views.HomeView.as_view(), name="home_view"),
    url(r'^product/$', views.ProductView.as_view(), name="product_view"),
    url(r'^cart/$', views.CartView.as_view(), name="cart_view"),
    url(r'^order/addressselect/$', views.AddressView.as_view(), name="address_view"),
    url(r'^order/payment/$', views.OrderPaymentView.as_view(), name="orderpayment_view"),
    url(r'^order-complete/$', csrf_exempt(views.OrderCompleteView.as_view()), name="order_success_view"),

    url(r'^admin-portal/$', views.AdminOrderView.as_view(), name="adminportal_view"),
    url(r'^redirect/$', RedirectView.as_view(url="/order-success/"), name="redirect_view"),

    url(r'^about-us/$', views.AboutUsView.as_view(), name="aboutus_view"),
    url(r'^terms/$', views.TermsConditionView.as_view(), name="terms_condition_view"),
    url(r'^privacy-policy/$', views.PrivacyPolicyView.as_view(), name="privacy_policy_view"),
    url(r'^refund-policy/$', views.RefundPolicyView.as_view(), name="refund_policy_view"),
    url(r'^contact-us/$', views.ContactUsView.as_view(), name="contact_view")
]
